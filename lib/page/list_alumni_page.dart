import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListAlumniPage extends StatefulWidget {
  final SharedPreferences sharedPreferences;
  final Dio dio;

  const ListAlumniPage({
    Key? key,
    required this.sharedPreferences,
    required this.dio,
  }) : super(key: key);

  @override
  _ListAlumniPageState createState() => _ListAlumniPageState();
}

class _ListAlumniPageState extends State<ListAlumniPage> with AfterLayoutMixin {
  bool isLoading = true;
  List<Map<String, dynamic>>? listAlumni;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Alumni"),
      ),
      body: Builder(
        builder: (context) {
          if (isLoading) {
            return buildLoading(context);
          } else if (listAlumni != null && listAlumni!.isNotEmpty) {
            return buildListView(context);
          } else {
            return buildNoData(context);
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "add_alumni").then((value) {
            requestData();
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    requestData();
  }

  void showMessage(String? text) {
    if (text != null && text.isNotEmpty) {
      Fluttertoast.showToast(msg: text);
    }
  }

  void showLoading() {
    setState(() {
      isLoading = true;
    });
  }

  void hideLoading() {
    setState(() {
      isLoading = false;
    });
  }

  Map<String, dynamic> buildHeader() {
    return {
      "Triton-Token": widget.sharedPreferences.getString("token"),
    };
  }

  void requestData() {
    showLoading();

    Map<String, dynamic> param = {
      "select": '["alumni_id","alumni_nrp","alumni_code","alumni_name","alumni_email"]',
      "ws": true,
    };

    widget.dio.post(
      "alumni/index.mod",
      data: FormData.fromMap(param),
      options: Options(
        headers: buildHeader(),
      ),
    ).then((Response response) {
      parseResponse(response);
    }).onError((error, stackTrace) {
      hideLoading();
      showMessage(error.toString());
    });
  }

  void parseResponse(Response response) {
    listAlumni = [];
    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["rows"] != null && data["rows"] is List) {
        for (dynamic raw in data["rows"]) {
          listAlumni!.add(raw as Map<String, dynamic>);
        }
      }
    }

    hideLoading();
  }

  Widget buildLoading(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildNoData(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: const Text("No Data Found"),
    );
  }

  Widget buildListView(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(8),
      child: ListView.builder(
        itemCount: listAlumni!.length,
        itemBuilder: (context, index) {
          Map<String, dynamic> alumni = listAlumni![index];

          return GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                "detail_alumni",
                arguments: alumni["alumni_id"],
              ).then((value) {
                requestData();
              });
            },
            child: Container(
              width: double.maxFinite,
              margin: const EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                      right: 8,
                    ),
                    child: const Icon(
                      Icons.account_circle_rounded,
                      size: 30,
                      color: Colors.blue,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          alumni["alumni_name"],
                          style: const TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(alumni["alumni_nrp"]),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
