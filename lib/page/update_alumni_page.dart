import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm_ppns2/custom_dropdown.dart';

class UpdateAlumniPage extends StatefulWidget {
  final SharedPreferences sharedPreferences;
  final Dio dio;
  final String id;

  const UpdateAlumniPage({
    Key? key,
    required this.sharedPreferences,
    required this.dio,
    required this.id,
  }) : super(key: key);

  @override
  _UpdateAlumniPageState createState() => _UpdateAlumniPageState();
}

class _UpdateAlumniPageState extends State<UpdateAlumniPage> with AfterLayoutMixin {
  bool isLoading = true;
  Map<String, dynamic>? alumni;

  late TextEditingController name;
  late TextEditingController email;
  late TextEditingController nrp;
  late TextEditingController alumniCode;

  List<Map<String, dynamic>> prodiOptions = [];
  List<Map<String, dynamic>> yearOptions = [];

  String? selectedProdi;
  String? selectedEntryYear;
  String? selectedGraduateYear;

  @override
  void initState() {
    super.initState();

    name = TextEditingController();
    email = TextEditingController();
    nrp = TextEditingController();
    alumniCode = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();

    name.dispose();
    email.dispose();
    nrp.dispose();
    alumniCode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Update Alumni"),
      ),
      body: Builder(
        builder: (context) {
          if(isLoading){
            return buildLoading(context);
          }
          else if(alumni != null && alumni!.isNotEmpty){
            return buildForm(context);
          }
          else{
            return buildNoData(context);
          }
        },
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    requestAllFormData();
  }

  void showMessage(String? text) {
    if (text != null && text.isNotEmpty) {
      Fluttertoast.showToast(msg: text);
    }
  }

  void showLoading() {
    setState(() {
      isLoading = true;
    });
  }

  void hideLoading() {
    setState(() {
      isLoading = false;
    });
  }

  Map<String, dynamic> buildHeader() {
    return {
      "Triton-Token": widget.sharedPreferences.getString("token"),
    };
  }

  void requestAllFormData(){
    FutureGroup<Response> futureGroup = FutureGroup();

    futureGroup.future.then((futures){
      parseCurrentResponse(futures[0]);
      parseProdiResponse(futures[1]);
      parseYearResponse(futures[2]);

      hideLoading();
    }).onError((error, stackTrace){
      hideLoading();

      showMessage(error.toString());
    });

    futureGroup.add(requestCurrentData());
    futureGroup.add(requestProdiData());
    futureGroup.add(requestYearData());
    futureGroup.close();
  }

  Future<Response> requestCurrentData() {
    return widget.dio.get(
      "alumni/detail.mod",
      queryParameters: {
        "primary_key": widget.id,
      },
      options: Options(
        headers: buildHeader(),
      ),
    );
  }

  void parseCurrentResponse(Response response) {
    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["row"] != null) {
        alumni = data["row"];

        name.text = alumni!["alumni_name"];
        email.text = alumni!["alumni_email"];
        nrp.text = alumni!["alumni_nrp"];
        alumniCode.text = alumni!["alumni_code"];
        selectedProdi = alumni!["alumni_program"];
        selectedEntryYear = alumni!["alumni_entryyear"];
        selectedGraduateYear = alumni!["alumni_graduateyear"];
      }
    }

    hideLoading();
  }

  Future<Response> requestProdiData() {
    Map<String, dynamic> param = {
      "select": '["program_id","program_name"]',
      "advsearch": '[{"logical":"and","data":[{"field_name":"program_collegeinstitution__collegeinstitution_id","operator":"e","value":["7422"]}],"children":[]}]',
      "ws": true,
    };

    return widget.dio.post(
      "program/index.mod",
      data: FormData.fromMap(param),
      options: Options(
        headers: buildHeader(),
      ),
    );
  }

  Future<Response> requestYearData() {
    Map<String, dynamic> param = {
      "select": '["year_id","year_value"]',
      "ws": true,
    };

    return widget.dio.post(
      "year/index.mod",
      data: FormData.fromMap(param),
      options: Options(
        headers: buildHeader(),
      ),
    );
  }

  void parseProdiResponse(Response response) {
    prodiOptions = [];

    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["rows"] != null && data["rows"] is List) {
        for (dynamic raw in data["rows"]) {
          prodiOptions.add(raw as Map<String, dynamic>);
        }
      }
    }
  }

  void parseYearResponse(Response response) {
    yearOptions = [];

    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["rows"] != null && data["rows"] is List) {
        for (dynamic raw in data["rows"]) {
          yearOptions.add(raw as Map<String, dynamic>);
        }
      }
    }
  }

  Widget buildLoading(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildNoData(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: const Text("No Data Found"),
    );
  }

  Widget buildForm(BuildContext context) {
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Builder(
            builder: (context) {
              return Column(
                children: [
                  TextFormField(
                    controller: name,
                    decoration: const InputDecoration(
                      hintText: "Nama",
                      labelText: "Nama",
                    ),
                    validator: (value) {
                      return (value != null && value.isNotEmpty)
                          ? null
                          : "Required";
                    },
                    maxLines: 1,
                  ),
                  TextFormField(
                    controller: email,
                    decoration: const InputDecoration(
                      hintText: "Email",
                      labelText: "Email",
                    ),
                    validator: (value) {
                      return (value != null && value.isNotEmpty)
                          ? null
                          : "Required";
                    },
                    maxLines: 1,
                  ),
                  TextFormField(
                    controller: nrp,
                    decoration: const InputDecoration(
                      hintText: "NRP",
                      labelText: "NRP",
                    ),
                    validator: (value) {
                      return (value != null && value.isNotEmpty)
                          ? null
                          : "Required";
                    },
                    maxLines: 1,
                  ),
                  TextFormField(
                    controller: alumniCode,
                    decoration: const InputDecoration(
                      hintText: "Kode Alumni",
                      labelText: "Kode Alumni",
                    ),
                    validator: (value) {
                      return (value != null && value.isNotEmpty)
                          ? null
                          : "Required";
                    },
                    maxLines: 1,
                  ),
                  CustomDropdown(
                    title: "Program Studi",
                    valueDataKey: "program_id",
                    labelDataKey: "program_name",
                    data: prodiOptions,
                    defaultValue: selectedProdi,
                    onChanged: (value) {
                      selectedProdi = value;
                    },
                  ),
                  CustomDropdown(
                    title: "Tahun Masuk",
                    valueDataKey: "year_id",
                    labelDataKey: "year_value",
                    data: yearOptions,
                    defaultValue: selectedEntryYear,
                    onChanged: (value) {
                      selectedEntryYear = value;
                    },
                  ),
                  CustomDropdown(
                    title: "Tahun Lulus",
                    valueDataKey: "year_id",
                    labelDataKey: "year_value",
                    data: yearOptions,
                    defaultValue: selectedGraduateYear,
                    onChanged: (value) {
                      selectedGraduateYear = value;
                    },
                  ),
                  Container(
                    width: double.maxFinite,
                    margin: const EdgeInsets.only(
                      top: 32,
                    ),
                    child: ElevatedButton(
                      child: const Text("SIMPAN"),
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        if (Form.of(context)!.validate()) {
                          submitData();
                        }
                      },
                    ),
                  ),
                ],
              );
            }
          ),
        ),
      ),
    );
  }

  void submitData() {
    showLoading();

    Map<String, dynamic> param = {
      "alumni_name": name.text,
      "alumni_email": email.text,
      "alumni_nrp": nrp.text,
      "alumni_code": alumniCode.text,
      "alumni_program": selectedProdi,
      "alumni_entryyear": selectedEntryYear,
      "alumni_graduateyear": selectedGraduateYear,
    };

    widget.dio.post(
      "alumni/update.mod",
      data: FormData.fromMap({
        "_json": jsonEncode(param),
        "primary_key": widget.id,
      }),
      options: Options(
        headers: buildHeader(),
      ),
    ).then((Response response) {
      parseSubmitResponse(response);
    }).onError((error, stackTrace) {
      hideLoading();
      showMessage(error.toString());
    });
  }

  void parseSubmitResponse(Response response) {
    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["success"] != null && data["success"] == true) {
        hideLoading();
        showMessage(data["message"]);
        Navigator.pop(context);
      } else if (data["message"] != null) {
        hideLoading();
        showMessage(data["message"]);
      } else {
        hideLoading();
        showMessage("Update failed");
      }
    } else {
      hideLoading();
      showMessage("Update failed");
    }
  }
}
