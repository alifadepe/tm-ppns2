import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailAlumniPage extends StatefulWidget {
  final SharedPreferences sharedPreferences;
  final Dio dio;
  final String id;

  const DetailAlumniPage({
    Key? key,
    required this.sharedPreferences,
    required this.dio,
    required this.id,
  }) : super(key: key);

  @override
  _DetailAlumniPageState createState() => _DetailAlumniPageState();
}

class _DetailAlumniPageState extends State<DetailAlumniPage> with AfterLayoutMixin {
  bool isLoading = true;
  Map<String, dynamic>? alumni;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail Alumni"),
        actions: [
          Container(
            margin: const EdgeInsets.only(
              right: 8,
            ),
            child: GestureDetector(
              onTap: () {
                requestDeleteData();
              },
              child: const Icon(Icons.delete),
            ),
          )
        ],
      ),
      body: Builder(
        builder: (context) {
          if(isLoading){
            return buildLoading(context);
          }
          else if(alumni != null && alumni!.isNotEmpty){
            return buildDataView(context);
          }
          else{
            return buildNoData(context);
          }
        },
      ),
      floatingActionButton: isLoading ? null : FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "update_alumni", arguments: widget.id).then((value){
            requestData();
          });
        },
        child: const Icon(Icons.edit),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    requestData();
  }

  void showMessage(String? text) {
    if (text != null && text.isNotEmpty) {
      Fluttertoast.showToast(msg: text);
    }
  }

  void showLoading() {
    setState(() {
      isLoading = true;
    });
  }

  void hideLoading() {
    setState(() {
      isLoading = false;
    });
  }

  Map<String, dynamic> buildHeader() {
    return {
      "Triton-Token": widget.sharedPreferences.getString("token"),
    };
  }

  void requestData() {
    showLoading();

    Map<String, dynamic> param = {
      "select": '["alumni_id","alumni_nrp","alumni_code","alumni_name","alumni_email","alumni_program__program_name","alumni_graduateyear__year_value","alumni_entryyear__year_value"]',
      "advsearch": '[{"logical":"and","data":[{"field_name":"alumni_id","operator":"e","value":["${widget.id}"]}],"children":[]}]',
      "ws": true,
    };

    widget.dio.post(
      "alumni/index.mod",
      data: FormData.fromMap(param),
      options: Options(
        headers: buildHeader(),
      ),
    ).then((Response response) {
      parseResponse(response);
    }).onError((error, stackTrace) {
      hideLoading();
      showMessage(error.toString());
    });

    // Map<String, dynamic> param = {
    //   "primary_key": widget.id,
    // };
    //
    // widget.dio.get(
    //   "alumni/detail.mod",
    //   queryParameters: param,
    //   options: Options(
    //     headers: buildHeader(),
    //   ),
    // ).then((Response response) {
    //   parseResponse(response);
    // }).onError((error, stackTrace) {
    //   hideLoading();
    //   showMessage(error.toString());
    // });
  }

  void parseResponse(Response response) {
    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["rows"] != null && data["rows"] is List) {
        alumni = data["rows"][0];
      }

      // if (data["row"] != null) {
      //   alumni = data["row"];
      // }
    }

    hideLoading();
  }

  void requestDeleteData() {
    showLoading();

    Map<String, dynamic> param = {
      "primary_key": '["${widget.id}"]',
      "bulk": true,
    };

    widget.dio.post(
      "alumni/del.mod",
      data: FormData.fromMap(param),
      options: Options(
        headers: buildHeader(),
      ),
    ).then((Response response) {
      parseDeleteResponse(response);
    }).onError((error, stackTrace) {
      hideLoading();
      showMessage(error.toString());
    });
  }

  void parseDeleteResponse(Response response) {
    if (response.data != null) {
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["message"] != null) {
        showMessage(data["message"]);
      }
      if (data["success"] != null && data["success"] == true) {
        Navigator.pop(context);
      }
    }

    hideLoading();
  }

  Widget buildLoading(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildNoData(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: const Text("No Data Found"),
    );
  }

  Widget buildDataView(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Icon(Icons.account_circle_rounded,
                  color: Colors.grey[500],
                  size: 20,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Nama",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[500],
                      ),
                    ),
                    Text(alumni!["alumni_name"] ?? "-",
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Icon(Icons.account_circle_rounded,
                  color: Colors.grey[500],
                  size: 20,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("NRP",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[500],
                      ),
                    ),
                    Text(alumni!["alumni_nrp"] ?? "-",
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Icon(Icons.account_circle_rounded,
                  color: Colors.grey[500],
                  size: 20,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Email",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[500],
                      ),
                    ),
                    Text(alumni!["alumni_email"] ?? "-",
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Icon(Icons.account_circle_rounded,
                  color: Colors.grey[500],
                  size: 20,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Program Studi",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[500],
                      ),
                    ),
                    Text(alumni!["alumni_program__program_name"] ?? "-",
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Icon(Icons.account_circle_rounded,
                  color: Colors.grey[500],
                  size: 20,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Tahun Masuk",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[500],
                      ),
                    ),
                    Text(alumni!["alumni_entryyear__year_value"] ?? "-",
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  right: 8,
                ),
                child: Icon(Icons.account_circle_rounded,
                  color: Colors.grey[500],
                  size: 20,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Tahun Lulus",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[500],
                      ),
                    ),
                    Text(alumni!["alumni_graduateyear__year_value"] ?? "-",
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
