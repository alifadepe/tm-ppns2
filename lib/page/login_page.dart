import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';

class LoginPage extends StatefulWidget {
  final SharedPreferences sharedPreferences;
  final Dio dio;

  const LoginPage({
    Key? key,
    required this.sharedPreferences,
    required this.dio,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late TextEditingController username;
  late TextEditingController password;
  late ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();

    progressDialog = ProgressDialog(context: context);
    username = TextEditingController();
    password = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();

    username.dispose();
    password.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.all(24),
          child: buildForm(context),
        ),
      ),
    );
  }

  Widget buildForm(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Builder(
        builder: (context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: username,
                decoration: const InputDecoration(
                  hintText: "Username",
                  prefixIcon: Icon(Icons.account_circle),
                ),
                validator: (value) {
                  return (value != null && value.isNotEmpty)
                      ? null
                      : "Required";
                },
                maxLines: 1,
              ),
              TextFormField(
                controller: password,
                decoration: const InputDecoration(
                  hintText: "Password",
                  prefixIcon: Icon(Icons.lock),
                ),
                validator: (value) {
                  return (value != null && value.isNotEmpty)
                      ? null
                      : "Required";
                },
                maxLines: 1,
                obscureText: true,
              ),
              Container(
                width: double.maxFinite,
                margin: const EdgeInsets.only(
                  top: 32,
                ),
                child: ElevatedButton(
                  child: const Text("LOGIN"),
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    if (Form.of(context)!.validate()) {
                      requestLogin();
                    }
                  },
                ),
              ),
            ],
          );
        }
      ),
    );
  }

  void showMessage(String? text) {
    if (text != null && text.isNotEmpty) {
      Fluttertoast.showToast(msg: text);
    }
  }

  void showLoading(){
    progressDialog.show(max: 1, msg: "Loading");
  }

  void hideLoading(){
    if(progressDialog.isOpen()){
      progressDialog.close();
    }
  }

  void requestLogin() {
    showLoading();

    Map<String, dynamic> param = {
      "username": username.text,
      "password": password.text,
      "mobile": 1,
    };

    widget.dio.post(
      "auth/login.json",
      data: FormData.fromMap(param),
    ).then((Response response) {
      validateLoginResponse(response);
    }).onError((error, stackTrace) {
      hideLoading();
      showMessage(error.toString());
    });
  }

  void validateLoginResponse(Response response) {
    if (response.data != null) {
      hideLoading();
      Map<String, dynamic> data = jsonDecode(response.data);

      if (data["success"] != null && data["success"] == true) {
        showMessage(data["message"]);
        saveLoginData(data);
        Navigator.pushReplacementNamed(context, "home");
      } else if (data["message"] != null) {
        showMessage(data["message"]);
      } else {
        showMessage("Login failed");
      }
    } else {
      hideLoading();
      showMessage("Login failed");
    }
  }

  void saveLoginData(Map<String, dynamic> data) {
    widget.sharedPreferences.setString("token", data["token"]);
    widget.sharedPreferences.setString("user", jsonEncode(data["user"]));
  }
}
