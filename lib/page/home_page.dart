import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  final SharedPreferences sharedPreferences;
  final Dio dio;

  const HomePage({
    Key? key,
    required this.sharedPreferences,
    required this.dio,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
      ),
      body: Container(
        padding: const EdgeInsets.all(24),
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, "list_alumni");
              },
              child: Column(
                children: const [
                  Icon(Icons.account_box, size: 40, color: Colors.blue,),
                  Text("Alumni"),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void showMessage(String? text) {
    if (text != null && text.isNotEmpty) {
      Fluttertoast.showToast(msg: text);
    }
  }

  // void showLoading(){
  //   progressDialog.show(max: 1, msg: "Loading");
  // }
  //
  // void hideLoading(){
  //   if(progressDialog.isOpen()){
  //     progressDialog.close();
  //   }
  // }

  void requestLogin() {
    // Map<String, dynamic> param = {
    //   "username": username.text,
    //   "password": username.text,
    //   "mobile": true,
    // };
    //
    // Map<String, dynamic> header = {
    //   "Triton-Token": username.text,
    //   "password": username.text,
    //   "mobile": true,
    // };
    //
    // widget.dio.post(
    //   "auth/login.json",
    //   data: param,
    //   options: Options(
    //     headers: header,
    //   ),
    // ).then((Response response) {
    //   validateLoginResponse(response);
    // }).onError((error, stackTrace) {
    //   showMessage(error.toString());
    // });
  }
}
