import 'package:flutter/material.dart';

class CustomDropdown extends StatefulWidget {
  final List<Map<String, dynamic>> data;
  final String labelDataKey;
  final String valueDataKey;
  final String title;
  final ValueChanged<String> onChanged;
  final String? defaultValue;

  const CustomDropdown({
    Key? key,
    required this.data,
    required this.labelDataKey,
    required this.valueDataKey,
    required this.title,
    required this.onChanged,
    this.defaultValue,
  }) : super(key: key);

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  late List<DropdownMenuItem<Map<String, dynamic>>> items;
  Map<String, dynamic>? selectedItem;

  @override
  void initState() {
    super.initState();

    items = buildItems();

    if(widget.defaultValue != null){
      selectedItem = findItemByValue(widget.defaultValue!);
    }
  }

  Map<String, dynamic>? findItemByValue(String value){
    for(Map<String, dynamic> map in widget.data){
      if(map[widget.valueDataKey] == value){
        return map;
      }
    }
  }

  List<DropdownMenuItem<Map<String, dynamic>>> buildItems() {
    return widget.data.map((Map<String, dynamic> data) {
      return DropdownMenuItem<Map<String, dynamic>>(
        value: data,
        child: Text(data[widget.labelDataKey]),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if(selectedItem != null)
          Container(
            margin: const EdgeInsets.only(
              top: 8,
            ),
            child: Text(
              widget.title,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 13,
                color: Colors.grey[700],
              ),
            ),
          ),

        DropdownButton<Map<String, dynamic>>(
          icon: const Icon(Icons.keyboard_arrow_down),
          isExpanded: true,
          elevation: 16,
          value: selectedItem,
          hint: Container(
            padding: const EdgeInsets.only(
              left: 8,
              top: 8,
              bottom: 16,
            ),
            child: Text(
              widget.title,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.grey[700],
              ),
            ),
          ),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (Map<String, dynamic>? data) {
            setState(() {
              selectedItem = data;
              widget.onChanged(data![widget.valueDataKey]);
            });
          },
          items: items,
        ),
      ],
    );
  }
}
