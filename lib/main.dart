import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tm_ppns2/page/add_alumni_page.dart';
import 'package:tm_ppns2/page/detail_alumni_page.dart';
import 'package:tm_ppns2/page/home_page.dart';
import 'package:tm_ppns2/page/list_alumni_page.dart';
import 'package:tm_ppns2/page/login_page.dart';
import 'package:tm_ppns2/page/update_alumni_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Dio dio = Dio(
    BaseOptions(
      baseUrl: "https://simv2.ppns.ac.id/index.php/",
      responseType: ResponseType.plain,
    ),
  );

  dio.interceptors.add(LogInterceptor(
    error: true,
    request: true,
    requestBody: true,
    requestHeader: true,
    responseBody: true,
    responseHeader: true,
    logPrint: (text) {
      log(text.toString());
    },
  ));

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  runApp(MyApp(
    dio: dio,
    sharedPreferences: sharedPreferences,
  ));
}

class MyApp extends StatelessWidget {
  final SharedPreferences sharedPreferences;
  final Dio dio;

  const MyApp({
    Key? key,
    required this.sharedPreferences,
    required this.dio,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Builder(
        builder: (context) {
          if(sharedPreferences.getString("token") != null){
            return HomePage(
              sharedPreferences: sharedPreferences,
              dio: dio,
            );
          }
          else{
            return LoginPage(
              sharedPreferences: sharedPreferences,
              dio: dio,
            );
          }
        },
      ),
      routes: {
        "home": (context){
          return HomePage(
            sharedPreferences: sharedPreferences,
            dio: dio,
          );
        },
        "login": (context){
          return LoginPage(
            sharedPreferences: sharedPreferences,
            dio: dio,
          );
        },
        "list_alumni": (context){
          return ListAlumniPage(
            sharedPreferences: sharedPreferences,
            dio: dio,
          );
        },
        "add_alumni": (context){
          return AddAlumniPage(
            sharedPreferences: sharedPreferences,
            dio: dio,
          );
        },
        "detail_alumni": (context){
          String id = ModalRoute.of(context)?.settings.arguments as String;

          return DetailAlumniPage(
            sharedPreferences: sharedPreferences,
            dio: dio,
            id: id,
          );
        },
        "update_alumni": (context){
          String id = ModalRoute.of(context)?.settings.arguments as String;

          return UpdateAlumniPage(
            sharedPreferences: sharedPreferences,
            dio: dio,
            id: id,
          );
        },
      },
    );
  }
}
